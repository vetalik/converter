from converters.converter import Converter


data = {
    "AUD/USD": 0.7754,
    "USD/CAD": 1.2330,
    "USD/CHF": 0.9313,
    "USD/DKK": 6.6336,
    "EUR/USD": 1.1231,
    "GBP/USD": 1.5620,
    "USD/HKD": 7.7524,
    "USD/JPY": 123.4440,
    "USD/NOK": 7.7762,
    "NZD/USD": 0.6980,
    "CKF/HRT": 0.222
}

conv = Converter()
conv.set_convertion_rates(data)
