from types import DictType


class ConvertionException(Exception):

    def __init__(self, source, dest):
        self.source = source
        self.dest = dest

    def __str__(self):
        return "{0} could not be converted to {1} with given data".format(
            self.source,
            self.dest
        )


class Converter:

    def __init__(self):
        self.convertion_rates = {}

    def set_convertion_rates(self, rates):
        assert type(rates) is DictType, "Convertion rates should be of DictType"
        self.convertion_rates = self._normalize_rates(rates)

    def _normalize_rates(self, rates):
        # Depending on incoming data structure we should normalize data for further work
        return [
            {
                'from': key.split('/')[0],
                'to': key.split('/')[1],
                'rate': value
            } for key, value in rates.iteritems()
        ]

    def _calculate(self, source, dest):
        rates = filter(lambda x: x["to"] == dest, self.convertion_rates)
        coeff = 0
        for rate in rates:
            if rate["from"] == source:
                coeff =  rate["rate"]
                return coeff
            else:
                a = self._calculate(source, rate["from"])
                if a == 0:
                    coeff = a
                else:
                    coeff = rate["rate"] * a
        return coeff

    def convert(self, source, dest, value):
        total_rate = 1;
        if source != dest:
            if filter(lambda x: x["from"] ==source, self.convertion_rates) and \
                filter(lambda x: x["to"] ==dest, self.convertion_rates):
                total_rate = self._calculate(source, dest)
            elif filter(lambda x: x["from"] ==dest, self.convertion_rates) and \
                filter(lambda x: x["to"] ==source, self.convertion_rates):
                rate = self._calculate(dest, source)
                if not rate:
                    raise ConvertionException(source, dest)
                total_rate = 1/rate
            else:
                raise ConvertionException(source, dest)
        if not total_rate:
            raise ConvertionException(source, dest)
        return value * total_rate
