import unittest

from converters import conv
from converters.converter import Converter, ConvertionException

class TestConverter(unittest.TestCase):

    def test_set_convertion_rates(self):
        data_sample = {
            "AUD/USD": 0.7754,
            "USD/CAD": 1.2330,
        }

        expected_data = [
            {
                "from": "AUD",
                "to": "USD",
                "rate": 0.7754
            },
            {
                "from": "USD",
                "to": "CAD",
                "rate": 1.2330
            }
        ]

        conv = Converter()
        conv.set_convertion_rates(data_sample)
        self.assertTrue(conv.convertion_rates)
        rates = list(conv.convertion_rates)
        for rate in rates:
            once = False
            for exp in expected_data:
                if exp["from"] == rate["from"] and exp["to"] == rate["to"] and exp["rate"] == rate["rate"]:
                    once = True
            self.assertTrue(once, "No equality for rate found: {0}".format(rate))

    def test_convert(self):
        expected_aud_to_cad = 10 * 0.7754 * 1.2330
        epselon = 0.0001
        self.assertTrue(abs(conv.convert("AUD", "CAD", 10) - expected_aud_to_cad)<epselon)
        expected_aud_to_jpy = 10 * 0.7754 * 123.4440
        self.assertTrue(conv.convert("AUD", "JPY", 10) - expected_aud_to_jpy < epselon)

    def test_convert_cad_aud(self):
        epselon = 0.0001
        expected = 10 * 1/(0.7754 * 1.2330)
        calculated = conv.convert("CAD", "AUD", 10)
        self.assertTrue(abs(calculated - expected)<epselon)

    def test_covert_cad_xxx(self):
        expected_exp = "{0} could not be converted to {1} with given data".format("CAD", "XXX")
        with self.assertRaises(ConvertionException) as ce:
            calculated = conv.convert("CAD", "XXX", 10)
        self.assertEqual(str(ce.exception), expected_exp)

    def test_aud_hrt(self):
        expected_exp = "{0} could not be converted to {1} with given data".format("AUD", "HRT")
        with self.assertRaises(ConvertionException) as ce:
            calculated = conv.convert("AUD", "HRT", 10)
        self.assertEqual(str(ce.exception), expected_exp)


if __name__ == '__main__':
    unittest.main()
